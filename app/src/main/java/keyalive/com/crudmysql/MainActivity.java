package keyalive.com.crudmysql;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener{

    private ListView listItem;
    String JSON_STRING;
    Context context;
    Koneksi koneksi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        context=this;
        getJson();
        listItem.setOnItemClickListener(this);

    }

    private void getJson() {
        class GetJson extends AsyncTask<String,Void,String> {
            ProgressDialog load;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                load=ProgressDialog.show(context,
                        "Otw Ngambiln",
                        "SABAR YAAA!",
                        false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                load.dismiss();
                JSON_STRING=s;
                showEmployee();
            }

            @Override
            protected String doInBackground(String... strings) {
                RequestHandler requestHandler= new RequestHandler();
                String s=requestHandler.senGetRequest(koneksi.URL_GET_ALL);
                return s;
            }
        }
        GetJson getJson= new GetJson();
        getJson.execute();
    }
        void showEmployee(){
            JSONObject jsonObject=null;
            ArrayList<HashMap<String,String>>list =new ArrayList<HashMap<String, String>>();
            try {
                jsonObject= new JSONObject(JSON_STRING);
                JSONArray result =jsonObject.getJSONArray(koneksi.TAG_JSON_ARRAY);
                for (int i=0;i<result.length();i++){
                    JSONObject jsO=result.getJSONObject(i);
                    String id=jsO.getString(koneksi.TAG_ID);
                    String name=jsO.getString(koneksi.TAG_NAME);

                    HashMap<String,String>employees=new HashMap<>();
                    employees.put(koneksi.TAG_ID,id);
                    employees.put(koneksi.TAG_NAME,name);
                    list.add(employees);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ListAdapter adapter = new SimpleAdapter(
                context,list,R.layout.list_item,
                    new String[]{koneksi.TAG_ID,Koneksi.TAG_NAME},
                    new int[]{R.id.id,R.id.name});
                listItem.setAdapter(adapter);
        }
    private void initView() {
        listItem = (ListView) findViewById(R.id.list_item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HashMap<String,String>map=(HashMap)parent.getItemAtPosition(position);
        String empId=map.get(koneksi.TAG_ID);
        startActivity(new Intent(context,TampilPegawai.class).putExtra(koneksi.TAG_ID,empId));

    }
}
